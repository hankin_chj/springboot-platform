package com.chj;

import com.chj.entity.Order;
import com.chj.entity.User;
import com.chj.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(classes = {App.class})
@RunWith(SpringRunner.class)
public class MoreDataBaseTest {
    @Resource
    private OrderService orderService;
    @Test
    public void test1() {
        User user = new User();
        user.setUserName("hankin1001");
        user.setPassword("123");
//        user.setId(1001);

        Order order = new Order();
        order.setAccount(001);
        order.setName("zhangsan");
        order.setUserId(1001);
        orderService.addOrder(order,user);
    }
}
