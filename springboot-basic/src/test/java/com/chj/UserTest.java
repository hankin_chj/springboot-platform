package com.chj;


import com.chj.dao.user.UserMapper;
import com.chj.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(classes = App.class)
@RunWith(SpringRunner.class)
public class UserTest {

    @Resource
    private UserMapper userMapper;

    @Test
    public void testAdd() {
        User user = new User();
        user.setPassword("123456");
        user.setUserName("hankin");
        userMapper.insertSelective(user);
    }

    @Test
    public void testFindUser() {
        User enjoy = userMapper.findUserByNamePasswd("hankin", "123456");
        System.out.println(enjoy);
    }

}
