package com.chj.dao.user;

import com.chj.entity.User;
import org.apache.ibatis.annotations.Param;

//@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User findUserByNamePasswd(@Param("userName") String userName, @Param("password") String password);

}