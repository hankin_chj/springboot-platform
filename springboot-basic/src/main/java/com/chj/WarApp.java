package com.chj;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * war部署方式需要 继承SpringBootServletInitializer
 */
@SpringBootApplication
@MapperScan("com.chj.dao")
@EnableTransactionManagement
public class WarApp extends SpringBootServletInitializer {
    public static void main(String[] args) {
        //启动类 定制化
        SpringApplication.run(WarApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(WarApp.class);
    }
}
