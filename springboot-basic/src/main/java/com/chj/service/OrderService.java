package com.chj.service;

import com.chj.entity.Order;
import com.chj.entity.User;

public interface OrderService {
    void addOrder(Order orders, User user);
}
