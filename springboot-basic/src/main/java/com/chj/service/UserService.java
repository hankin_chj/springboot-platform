package com.chj.service;

public interface UserService {
    boolean login(String username, String password);

    boolean register(String username, String password);

    void batchAdd(String username, String passwd);
}
