package com.chj.service.impl;

import com.chj.dao.user.UserMapper;
import com.chj.entity.User;
import com.chj.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper usersMapper;

    @Override
    public boolean login(String username, String passwd) {
        User user = usersMapper.findUserByNamePasswd(username, passwd);
        return user != null;
    }

    @Override
    public boolean register(String username, String passwd) {
        User user = new User();
        user.setUserName(username);
        user.setPassword(passwd);
        int cnt = usersMapper.insert(user);
        return cnt > 0;
    }

    @Override
    public void batchAdd(String username, String passwd) {
        User user = new User();
        user.setUserName(username);
        user.setPassword(passwd);
        usersMapper.insertSelective(user);
        int i = 10 / 0;
        user = new User();
        user.setUserName(username + "2");
        user.setPassword(passwd);
        usersMapper.insertSelective(user);
    }
}
