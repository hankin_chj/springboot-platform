package com.chj.service.impl;

import com.chj.dao.order.OrderMapper;
import com.chj.dao.user.UserMapper;
import com.chj.entity.Order;
import com.chj.entity.User;
import com.chj.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 *
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private OrderMapper orderMapper;

//    @Transactional
    @Override
    public void addOrder(Order order, User user) {
        orderMapper.insert(order);
//        int i=10/0;
        userMapper.insertSelective(user);
    }
}
