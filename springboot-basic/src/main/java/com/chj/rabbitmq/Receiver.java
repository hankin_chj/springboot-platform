package com.chj.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "hankin") //TODO 定义该类需要监听的队列
public class Receiver {
    // 指定对消息的处理
    @RabbitHandler
    public void process(String msg) {
        System.out.println("receive msg : " + msg);
    }
}
