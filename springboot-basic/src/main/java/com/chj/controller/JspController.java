package com.chj.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * jsp视图解析测试
 */
@Controller
@RequestMapping("/jsp")
public class JspController {
    @RequestMapping("/hello")
    public String sayHello() {
        return "index";
    }
}
