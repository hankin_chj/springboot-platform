package com.chj.controller;

import com.chj.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Resource
    private UserService userService;

    @RequestMapping("/hello")
    public Object sayHello() {
        logger.info("这是个hello的info日志!");
        logger.debug("这是个hello的debug日志!!");
        return "hello4";
    }

    @RequestMapping("/login")
    public String login(String username, String passwd) {
        boolean login = userService.login(username, passwd);
        if (login) {
            return "登陆成功";
        } else {
            return "登陆失败";
        }
    }

    @RequestMapping("/register")
    public String register(String username, String passwd) {
        boolean login = userService.register(username, passwd);
        if (login) {
            return "注册成功";
        } else {
            return "注册失败";
        }
    }

    //    @Transactional
    @RequestMapping("/batchAdd")
    public String batchAdd(String username, String passwd) {
        userService.batchAdd(username, passwd);
        return "成功";
    }

}
