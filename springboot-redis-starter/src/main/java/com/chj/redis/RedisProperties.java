package com.chj.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * 用于加载Redis需要的配置
 */
@ConfigurationProperties(prefix = "redis")
public class RedisProperties {
    private String host;
    private int port;
//    private String password;
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
}
